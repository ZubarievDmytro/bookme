import React from 'react';
import { screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';
import { useDispatch } from 'react-redux';
import User from './User';
import renderWithProvider from '../../../setupTests';

jest.mock('react-redux', () => {
  const { useSelector } = jest.requireActual('react-redux');

  return {
    useDispatch: jest.fn(),
    useSelector,
  };
});

const initialState = {
  usersCatalog: {
    usersList: {},
  },
  userPage: {
    fetchedUser: {},
  },
  auth: {
    user: {},
  },
};

const testUser = {
  id: 12345,
  name: 'Test user',
  description: 'Test description',
  schedule: [10, 20],
};
describe('User page', () => {
  describe('User without data', () => {
    const dispatchMock = jest.fn();
    beforeEach(() => {
      useDispatch.mockReturnValue(dispatchMock);
      renderWithProvider(<User match={{ params: { id: 12345 } }} />, {
        initialState,
      });
    });

    it('should load user if there is no user preloaded and display loader', () => {
      expect(dispatchMock).toBeCalledTimes(1);
      expect(document.querySelector('.loader')).toBeInTheDocument();
    });
  });

  describe('Preloaded user with shedule', () => {
    const dispatchMock = jest.fn(() => Promise.resolve());
    beforeEach(() => {
      useDispatch.mockReturnValue(dispatchMock);

      renderWithProvider(<User match={{ params: { id: 12345 } }} />, {
        initialState: {
          ...initialState,
          usersCatalog: {
            usersList: {
              12345: testUser,
            },
          },
          auth: {
            user: {
              id: 1,
              name: 'Test 2',
            },
          },
        },
      });
    });

    it('should disable save booking until both time and date choosen', () => {
      expect(screen.getByText('Save Booking')).toHaveClass('disabled');
      const chooseDate = document.querySelector('td:not(.disabled)');
      const chooseTime = screen.getByRole('option', { name: '11:00' });
      userEvent.click(chooseDate);
      userEvent.click(chooseTime);

      expect(screen.getByText('Save Booking')).not.toHaveClass('disabled');
    });

    it('should save booking', async () => {
      const chooseDate = document.querySelector('td:not(.disabled)');
      const chooseTime = screen.getByRole('option', { name: '11:00' });
      const saveButton = screen.getByText('Save Booking');

      // Chose date and time
      userEvent.click(chooseDate);
      userEvent.click(chooseTime);

      // Save booking
      userEvent.click(saveButton);

      // Show success message
      expect(await screen.findByTestId('message')).toBeInTheDocument();

      expect(dispatchMock).toBeCalledTimes(1);
    });

    it('should change UI when DateSelector changed', () => {
      const chooseItem = document.querySelector('td:not(.disabled)');

      userEvent.click(chooseItem);

      expect(chooseItem).toHaveClass('active');
    });

    it('should change UI when TimeSelector changed', () => {
      // default value
      expect(screen.getByRole('alert')).toHaveTextContent('Select your time');

      userEvent.click(screen.getByRole('option', { name: '11:00' }));

      expect(screen.getByRole('alert')).toHaveTextContent('11:00');
    });

    it('should load DateSelector and TimeSelector components on page', () => {
      expect(screen.getByTestId('dateSelector')).toBeInTheDocument();
      expect(screen.getByTestId('timeSelector')).toBeInTheDocument();
    });
    it('should display user if user exist in store', () => {
      expect(document.querySelectorAll('.card img').length).toEqual(1);
      expect(screen.getByText('Test user')).toBeInTheDocument();
      expect(screen.getByText('Test description')).toBeInTheDocument();
    });
  });

  describe('Preloaded user without shedule', () => {
    beforeEach(() => {
      renderWithProvider(<User match={{ params: { id: 12345 } }} />, {
        initialState: {
          ...initialState,
          usersCatalog: {
            usersList: {
              12345: { ...testUser, schedule: null },
            },
          },
        },
      });
    });

    it('should disable timeSelector and display message when user without shedule', () => {
      // default value
      expect(screen.getByRole('heading')).toHaveTextContent(
        'No available time'
      );

      expect(screen.getByTestId('timeSelector')).toHaveClass('disabled');
    });
  });
});
