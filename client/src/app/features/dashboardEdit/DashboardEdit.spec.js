import React from 'react';
import { act, screen, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';
import { useDispatch } from 'react-redux';
import DashboardEdit from './DashboardEdit';
import renderWithProvider from '../../../setupTests';

jest.mock('react-redux', () => {
  const { useSelector } = jest.requireActual('react-redux');

  return {
    useDispatch: jest.fn(),
    useSelector,
  };
});

const initialStateWithoutUser = {
  auth: {
    user: {
      bookings: [],
    },
  },
};
const initialState = {
  usersCatalog: {
    usersList: {},
  },
  userPage: {
    fetchedUser: {},
  },
  auth: {
    user: {
      id: 12345,
      name: 'Test user',
      description: 'Test description',
      bookings: [],
      schedule: ['5', '23'],
      avatarUrl: '',
      visible: false,
    },
    token: '12345',
    bookings: [],
  },
  form: {
    userForm: {
      values: {
        name: 'Test',
        profession: 'Test',
        description: 'Test',
      },
    },
  },
};

const match = {
  params: {
    id: 1,
  },
};

describe('Dashboard Edit', () => {
  describe('Without user info', () => {
    it('should show loader', () => {
      renderWithProvider(<DashboardEdit match={match} />, {
        initialStateWithoutUser,
      });
      expect(document.querySelector('.loader.active')).toBeInTheDocument();
    });
  });

  describe('Dashboard with user info', () => {
    const promise = Promise.resolve({ redirectTo: '/dashboard' });
    useDispatch.mockReturnValue(jest.fn(() => promise));

    const originalWarn = console.warn.bind(console.warn);

    beforeAll(() => {
      console.warn = (msg) =>
        !msg.toString().includes('componentWillReceiveProps') &&
        !msg.toString().includes('componentWillMount') &&
        originalWarn(msg);
    });

    afterAll(() => {
      console.warn = originalWarn;
    });

    beforeEach(() => {
      renderWithProvider(<DashboardEdit match={match} />, { initialState });
    });

    it('should render DashboardEdit with form, save button and delete account button', async () => {
      expect(document.querySelector('form')).toBeInTheDocument();
      expect(screen.getByText('Save Information')).toBeInTheDocument();
      expect(screen.getByText('Delete Account')).toBeInTheDocument();
    });

    it('should open modal on click "x" icon and close on click No', () => {
      userEvent.click(screen.getByText('Delete Account'));
      expect(
        screen.getByText('Are you sure you want to delete your account?')
      ).toBeInTheDocument();

      expect(
        document.querySelectorAll('.modals.visible.active').length
      ).toEqual(1);

      userEvent.click(screen.getByText('No'));

      expect(document.querySelectorAll('.modals').length).toEqual(0);
    });

    it('should remove user if clicked on Yes in modal', async () => {
      userEvent.click(screen.getByText('Delete Account'));

      userEvent.click(screen.getByText('Yes'));

      await waitFor(() => expect(useDispatch).toHaveBeenCalled());
      await act(() => promise);
    });

    it('should dispatch save information', async () => {
      userEvent.type(screen.getByLabelText('Full Name'), 'Test Full Name');
      userEvent.type(screen.getByLabelText('Description'), 'Test Description');
      userEvent.type(screen.getByLabelText('Job Title'), 'Test Job Title');
      userEvent.click(screen.getByText('Save Information'));

      await waitFor(() => expect(useDispatch).toHaveBeenCalled());

      await act(() => promise);
    });
  });
});
