import React from 'react';
import { screen, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';
import UserForm from '.';
import renderWithProvider from '../../../../../setupTests';

const initialState = {
  usersCatalog: {
    usersList: {},
  },
  userPage: {
    fetchedUser: {},
  },
  auth: {
    user: {
      id: 12345,
      name: 'Test user',
      description: 'Test description',
      bookings: [],
      schedule: ['5', '23'],
      avatarUrl: '',
      visible: false,
    },
    token: '12345',
    bookings: [],
  },
  form: {
    userForm: {
      values: {
        name: 'Test',
        profession: 'Test',
        description: 'Test',
      },
    },
  },
};

const match = {
  params: {
    id: 1,
  },
};

describe('UserForm', () => {
  const originalWarn = console.warn.bind(console.warn);
  const onSubmit = jest.fn((formValues) => formValues);
  beforeAll(() => {
    console.warn = (msg) =>
      !msg.toString().includes('componentWillReceiveProps') &&
      !msg.toString().includes('componentWillMount') &&
      originalWarn(msg);
  });

  afterAll(() => {
    console.warn = originalWarn;
  });
  beforeEach(() => {
    renderWithProvider(<UserForm match={match} onSubmit={onSubmit} />, {
      initialState,
    });
  });

  it('should render DashboardEdit with form, save button and delete account button', async () => {
    expect(document.querySelector('form')).toBeInTheDocument();
    expect(screen.getByText('Save Information')).toBeInTheDocument();
  });

  it('should dispatch save information', async () => {
    userEvent.type(screen.getByLabelText('Full Name'), 'Test Full Name');
    userEvent.type(screen.getByLabelText('Description'), 'Test Description');
    userEvent.type(screen.getByLabelText('Job Title'), 'Test Job Title');
    userEvent.click(screen.getByText('Save Information'));

    await waitFor(() => expect(onSubmit).toHaveBeenCalled());

    expect(onSubmit).toHaveBeenCalledWith({
      description: 'Test Description',
      name: 'Test Full Name',
      profession: 'Test Job Title',
    });
  });

  it('should show error if user clear the input field', () => {
    expect(
      screen.queryByText('You must enter a Full Name')
    ).not.toBeInTheDocument();
    userEvent.type(screen.getByLabelText('Full Name'), 'Test Full Name');
    userEvent.clear(screen.getByLabelText('Full Name'));
    userEvent.click(screen.getByLabelText('Description'));
    expect(screen.getByText('You must enter a Full Name')).toBeInTheDocument();
  });
});
