import React from 'react';
import '@testing-library/jest-dom';
import { screen, render } from '@testing-library/react';
import { Router } from 'react-router-dom';
import UsersList from './UsersList';
import history from '../../../../../history';

const users = [
  { id: 1, name: 'Test Name', description: 'Test description' },
  { id: 2, name: 'John Test', description: 'Test description 2' },
];
const userId = 1;

describe('UsersList', () => {
  it('should render a list of users', () => {
    render(
      <Router history={history}>
        <UsersList users={users} userId={userId} />
      </Router>
    );

    expect(screen.getAllByRole('img').length).toEqual(2);
    expect(screen.getAllByRole('link').length).toEqual(2);
    expect(screen.getAllByRole('button').length).toEqual(1);
  });

  it('should show loader if there is no users', () => {
    render(<UsersList users={[]} />);

    expect(document.querySelector('.loader.active')).toBeInTheDocument();
  });
});
