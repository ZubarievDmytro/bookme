import React from 'react';
import { Card, Button, Loader } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import UserCard from '../../../../shared/components/userCard';

const UsersList = ({ users, userId }) => {
  const renderUsers = () => {
    if (!users.length) return <Loader active size="large" />;

    return users.map((user) => {
      return (
        <UserCard key={user.id} user={user} as={Link} to={`/users/${user.id}`}>
          {userId !== user.id && (
            <Card.Content extra>
              <Button basic color="green" floated="right" content="Book me" />
            </Card.Content>
          )}
        </UserCard>
      );
    });
  };

  return <Card.Group itemsPerRow={3}>{renderUsers()}</Card.Group>;
};

export default UsersList;
