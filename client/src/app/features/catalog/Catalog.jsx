import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import UsersList from './components/usersList';
import { fetchUsers, selectUsers } from './catalogSlice';

const Catalog = () => {
  const userId = localStorage.getItem('userId');
  const users = Object.values(useSelector(selectUsers));
  const dispatch = useDispatch();

  useEffect(() => {
    if (!users.length) dispatch(fetchUsers());
  }, [dispatch, users.length]);

  return <UsersList users={users} userId={userId} />;
};

export default Catalog;
