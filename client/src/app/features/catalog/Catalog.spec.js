import React from 'react';
import { useDispatch } from 'react-redux';
import Catalog from './Catalog';
import renderWithProvider from '../../../setupTests';

const initialState = {
  usersCatalog: {
    usersList: {},
  },
};
jest.mock('react-redux', () => {
  const { useSelector } = jest.requireActual('react-redux');

  return {
    useDispatch: jest.fn(),
    useSelector,
  };
});

describe('Catalog', () => {
  it('should try to load users if no users in the store', () => {
    const dummyDispatch = jest.fn();
    useDispatch.mockReturnValue(dummyDispatch);
    renderWithProvider(<Catalog />, { initialState });

    expect(dummyDispatch).toHaveBeenCalled();
  });
});
