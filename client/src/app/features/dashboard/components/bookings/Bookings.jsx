import React, { useState, useEffect } from 'react';
import { Card, Icon, Message } from 'semantic-ui-react';
import UserModal from '../../../../shared/components/userModal/UserModal';

const Bookings = ({ signedInBookings, title, bookings, onDeleteBooking }) => {
  const [book, setBook] = useState(null);
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState({ status: '', text: '' });

  useEffect(() => {
    let timeout;
    if (message.text) {
      timeout = setTimeout(() => {
        setMessage({ status: '', text: '' });
      }, 4000);
    }

    return () => clearTimeout(timeout);
  }, [message.text]);

  const onDeleteClick = (booking) => {
    setOpen(true);
    setBook(booking);
  };

  const onModalClick = (status) => {
    if (status === 'yes') {
      setOpen(false);
      setBook(null);
      onDeleteBooking(book)
        .then(() => {
          setMessage({
            status: 'success',
            text: `You successfully removed ${book.user.name} on ${book.time}:00 ${book.date}`,
          });
        })
        .catch(() => {
          setMessage({
            status: 'negative',
            text: 'Something went wrong. Please try again.',
          });
        });
    } else {
      setOpen(false);
      setBook(null);
    }
  };

  const renderTime = (booking) => {
    const formatedTime = `${booking.time}:00 - ${+booking.time + 1}:00`;
    return (
      <p>
        {formatedTime}{' '}
        {signedInBookings && (
          <Icon
            onClick={() => onDeleteClick(booking)}
            floated="right"
            color="red"
            name="window close outline"
            data-testid="delete_booking"
          />
        )}
      </p>
    );
  };

  const renderContent = () => {
    if (bookings.length) {
      return (
        <>
          <h3>{title}</h3>
          <Card.Group itemsPerRow={3}>
            {bookings.map((bookItem) => {
              const userName = signedInBookings
                ? bookItem.user.name
                : bookItem.name;
              const { date } = bookItem;
              return (
                <Card key={bookItem.id}>
                  <Card.Content>
                    <Card.Header>{userName}</Card.Header>
                    <Card.Meta>{date}</Card.Meta>
                    <Card.Description>{renderTime(bookItem)}</Card.Description>
                  </Card.Content>
                </Card>
              );
            })}
          </Card.Group>
        </>
      );
    }
    return (
      <>
        <h3>{title}</h3>
        <Card>
          <Card.Content>No bookings yet</Card.Content>
        </Card>
      </>
    );
  };

  return (
    <div>
      <Message
        className={message.status}
        content={message.text}
        hidden={!message.text}
      />
      {renderContent()}
      <UserModal open={open} text="booking" onModalClick={onModalClick} />
    </div>
  );
};

export default Bookings;
