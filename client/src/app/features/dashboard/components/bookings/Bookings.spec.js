import React from 'react';
import { screen, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';
import { act } from 'react-dom/test-utils';
import Bookings from './Bookings';

const bookings = [
  {
    user: {
      id: 2,
      name: 'Test User 1',
    },
    date: '01-01-2021',
    email: 'test2@gmail.com',
    id: '1',
    name: 'Test User 2',
    time: '6',
  },
];
describe('Bookings', () => {
  it('should render bookings title and No bookings yet', async () => {
    render(<Bookings title="Test bookings" bookings={[]} />);

    expect(screen.getByText('Test bookings')).toBeInTheDocument();
    expect(screen.getByText('No bookings yet')).toBeInTheDocument();
  });

  it('should render bookings list', () => {
    render(<Bookings title="Test bookings" bookings={bookings} />);
    expect(document.querySelector('.card')).toBeInTheDocument();
  });

  it('should open modal on click "x" icon and close on click No', () => {
    render(
      <Bookings signedInBookings title="Test bookings" bookings={bookings} />
    );
    // Click to open modal
    userEvent.click(screen.getByTestId('delete_booking'));
    expect(
      screen.getByText('Are you sure you want to delete your booking?')
    ).toBeInTheDocument();
    expect(document.querySelectorAll('.modals.visible.active').length).toEqual(
      1
    );
    // Click No to close modal
    userEvent.click(screen.getByText('No'));
    expect(document.querySelectorAll('.modals').length).toEqual(0);
  });

  it('should open modal on click "x" icon and delete on click Yes', async () => {
    const promise = Promise.resolve();
    const onDeleteBooking = jest.fn(() => promise);
    render(
      <Bookings
        signedInBookings
        title="Test bookings"
        bookings={bookings}
        onDeleteBooking={onDeleteBooking}
      />
    );
    // Click to open modal
    userEvent.click(screen.getByTestId('delete_booking'));

    // Click No to close modal
    userEvent.click(screen.getByText('Yes'));

    expect(onDeleteBooking).toBeCalledWith(bookings[0]);
    await act(() => promise);
  });
});
