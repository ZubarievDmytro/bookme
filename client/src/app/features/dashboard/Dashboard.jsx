import React from 'react';
import _ from 'lodash';
import { Link, Redirect } from 'react-router-dom';
import { Button, Grid, Loader } from 'semantic-ui-react';
import { useSelector, useDispatch } from 'react-redux';
import Bookings from './components/bookings';
import UserCard from '../../shared/components/userCard';
import {
  deleteBooking,
  selectFetchedBookings,
  selectSignedInUserBookings,
} from '../../shared/components/authForm/authSlice';

const Dashboard = () => {
  const token = localStorage.getItem('token');
  const { user } = useSelector((state) => state.auth);

  const dispatch = useDispatch();
  const onDeleteBooking = (book) => dispatch(deleteBooking(book, token));
  const fetchedBookings = useSelector(selectFetchedBookings);
  const signedInUserBookings = useSelector(selectSignedInUserBookings);

  const renderContent = () => {
    if (_.isEmpty(user)) return <Loader active size="large" />;

    return (
      <Grid columns="two" divided>
        <Grid.Column width={5}>
          <UserCard
            user={user}
            config={{
              location: 'outside',
            }}
          >
            <Button
              content="Edit profile information"
              as={Link}
              to={`/dashboard/edit/${user.id}`}
            />
          </UserCard>
        </Grid.Column>
        <Grid.Column width={11}>
          <Bookings
            signedInBookings
            title="My Bookings"
            bookings={fetchedBookings}
            onDeleteBooking={onDeleteBooking}
          />
          <Bookings
            title="Users booked me"
            bookings={signedInUserBookings}
            onDeleteBooking={onDeleteBooking}
          />
        </Grid.Column>
      </Grid>
    );
  };

  return (
    <div>
      {!token && <Redirect to="/" />}
      <h1>Dashboard</h1>
      {renderContent()}
    </div>
  );
};

export default Dashboard;
