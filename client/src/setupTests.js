import React from 'react';
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import { render } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import { reducer as formReducer } from 'redux-form';
import catalogReducer from './app/features/catalog/catalogSlice';
import userReducer from './app/features/user/userSlice';
import authReducer from './app/shared/components/authForm/authSlice';

const renderWithProvider = (
  ui,
  {
    initialState,
    store = configureStore({
      reducer: {
        usersCatalog: catalogReducer,
        userPage: userReducer,
        form: formReducer,
        auth: authReducer,
      },
      preloadedState: initialState,
    }),
    ...renderOptions
  } = {}
) => {
  function Wrapper({ children }) {
    return (
      <Provider store={store}>
        <Router>{children} </Router>
      </Provider>
    );
  }
  return render(ui, { wrapper: Wrapper, ...renderOptions });
};

export default renderWithProvider;
