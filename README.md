## BookMeNow

BookMeNow is an app where you can see the list of people and have possibility to search for a specific person. If you want to book someone you need to register using your email.

After registering in your dashboard you can change an information about yourself and manage your bookings.

If you want you also can make your profile visible and set a schedule for people who want to book your piece of time.

## Demo

You can see how it works via link:

http://bookmenow.zubariev-dmytro.com/

## Technology stack

### Client

- ReactJS
- Redux (ReduxToolkit)
- Semantic UI React

### Server

- Express
- MongoDB
- Passport
- JWT Simple

## Installation

### To run this project you need to do a few little steps:

- Clone project to your folder:

  ```bash
  git clone https://gitlab.com/ZubarievDmytro/bookme YOUR_FOLDER_NAME_HERE
  ```

- Go to client folder:

  ```bash
  cd YOUR_FOLDER_NAME_HERE/client
  ```

- Install dependencies using yarn:

  ```bash
  yarn
  ```

- Run project:

  ```bash
  yarn start
  ```

- That's it! App should now be running on http://localhost:3000/
